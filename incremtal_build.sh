#! /bin/bash

for max in {1..15}
do
	build=build$max
	mkdir $build
	max=$max*$max
	min=-$max
	time make COMP=g++ MAX=$max MIN=$min BUILD=./$build/
done
