ifndef COMP
	COMP = clang++
endif


ifndef MAX
	MAX = 20
endif
ifndef MIN
	MIN = -20
endif

CFLAGS = -std=c++17 -O0 -ftemplate-depth=200000 -D MAX=$(MAX) -D MIN=$(MIN)

ifndef NAME
	NAME = calculator
endif

ifndef SRC
	SRC = ./
endif

ifndef BUILD
	BUILD = ./
endif

CPP = $(wildcard $(SRC)*.cpp)

OBJ = $(CPP:$(SRC)%.cpp=$(BUILD)%.o)

$(BUILD)$(NAME): $(OBJ)
	$(COMP) -o $(BUILD)$(NAME) $(OBJ) $(LFLAGS)

-include $(OBJ:%.o=%.d)

$(BUILD)%.o: $(SRC)%.cpp
	$(COMP) $(CFLAGS) -c $(SRC)$*.cpp -o $(BUILD)$*.o
	echo -n $(BUILD) > $(BUILD)$*.d
	$(COMP) $(CFLAGS) -MM $(SRC)$*.cpp >> $(BUILD)$*.d

.PHONY : clean
.PHONY : test

test: $(BUILD)$(NAME)
	$(BUILD)$(NAME)

clean:
	rm $(BUILD)*.o $(BUILD)*.d $(BUILD)$(NAME)
