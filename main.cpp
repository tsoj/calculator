#include <iostream>
#include <string>
#include <optional>

template<int n1, int n2, int min, int max>
std::optional<std::string> calc(
  const std::string& first_number,
  const std::string& second_number,
  const std::string& op
)
{
  if constexpr(n1 < min)
  {
    return std::nullopt;
  }
  else if constexpr(n2 < min)
  {
    return calc<n1-1, max, min, max>(first_number, second_number, op);
  }
  else
  {
    if(first_number == std::to_string(n1) && second_number == std::to_string(n2))
    {
      switch (op[0]) {
        case '+': return std::to_string(n1 + n2);
        case '-': return std::to_string(n1 - n2);
        case '*': return std::to_string(n1 * n2);
        case '/': if constexpr(n2 == 0){return "\u2658";}else{return std::to_string(n1 / n2);}
      }
    }
    return calc<n1, n2-1, min, max>(first_number, second_number, op);
  }
}

template<int min, int max>
std::optional<std::string> calc(
  const std::string& first_number,
  const std::string& second_number,
  const std::string& op
)
{
  return calc<max, max, min, max>(first_number, second_number, op);
}

#ifndef MAX
	constexpr int MAX = 5;
#endif
#ifndef MIN
	constexpr int MIN = -5;
#endif

int main()
{
  std::string first_number, second_number, op;
  std::cout << "Enter first number: " << std::flush;
  std::getline(std::cin, first_number, '\n');
  std::cout << "Enter operator: " << std::flush;
  std::getline(std::cin, op, '\n');
  std::cout << "Enter second number: " << std::flush;
  std::getline(std::cin, second_number, '\n');

  std::cout << calc<MIN, MAX>(first_number, second_number, op).value() << std::endl;
}
